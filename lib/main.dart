import 'dart:async';
import 'dart:convert';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fl_country_code_picker/fl_country_code_picker.dart';
import 'package:intl_phone_field/countries.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Example', home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
   String numberEdit = '';
  final numberEditingController = TextEditingController();
  @override
 

  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    return Scaffold(
      body:Stack(
        alignment: Alignment.bottomCenter,
        children:<Widget>[
        Container(
          width: double.infinity,
          height: size.height*10/10,
          child:PreferredSize(
        preferredSize: const Size(double.infinity, 230),
        // tang kich thuoc chieu cao của appBar
        // here the desired height
        child: AppBar(
          
          flexibleSpace: Padding(
            padding: const EdgeInsets.all(80),
            //chia khoang cach tat ca deu bang 80
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    SvgPicture.asset('assets/a.svg'),
                    SvgPicture.asset('assets/b.svg'),
                  ],
                ),
                Column(
                  children: const [
                    Text(
                      'Recovered',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                    Text(
                      'Health',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ),
          backgroundColor: Color.fromRGBO(4, 25, 103, 1),
          
        ),
      ),
        ),
        Container(
          width: double.infinity,
          height: size.height *7/10,
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
          color: Colors.white),
          
          
          child: Wrap(
        alignment: WrapAlignment.spaceBetween,
        
        children: [
         
          Column(
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(51, 30, 52, 18),
              ),
              Text(
                'Enter Your Phone Number',
                style: TextStyle(fontSize: 20, color:Color.fromRGBO(4, 25, 103, 1)),
              ),
              Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceBetween,
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(51, 10, 52, 16),
              ),
              RichText(
                text: const TextSpan(
                  text: 'We will send you the  ',
                  style: TextStyle(color: Colors.black, fontSize: 15),
                  children: <TextSpan>[
                    TextSpan(
                        text: '6 digit ',
                        style:
                            TextStyle(color:Color.fromRGBO(4, 25, 103, 1), fontSize: 15)),
                    TextSpan(
                      text: 'verification',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    )
                  ],
                ),
              ),
              Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceBetween,
              ),
              const Text('code'),
              Padding(
                padding: EdgeInsets.fromLTRB(51, 10, 52, 16),
                child: TextField(
                  maxLength: 7,
                  //dinh dang cho chieu dai toi da la 7
                  controller: numberEditingController,
                  onChanged: (text) {
                    setState(
                      () {
                        numberEdit = text;
                      },
                    );
                  },
                  keyboardType: const TextInputType.numberWithOptions(),
                  decoration: const InputDecoration(
                    prefixIcon: Icon(Icons.phone),
                    hintText: 'Your Phone Number',
                  ),
                ),
              ),
              ButtonTheme(
                minWidth: 300.0,
                height: 40.0,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => SecondScreen(
                               numberEdit:numberEdit,
                                    //nhận dữ liệu numberEddit truyền sang cho SecondScreen kế thừa.
                              )),
                    );
                  },
                  child: const Text(
                    "NEXT",
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
        )
        
      ],
      )
     
      
      
    );
  }
}

class SecondScreen extends StatefulWidget {
  @override
  String numberEdit='';

  SecondScreen({Key? key,required this.numberEdit}): super(key: key);

  // ignore: library_private_types_in_public_api
  _SecondScreen createState() => _SecondScreen();
}

class _SecondScreen extends State<SecondScreen> {
final TextEditingController phoneController=TextEditingController();
final contryPicker=FlCountryCodePicker();
CountryCode?countryCode;
 String beginNumber='';
 String numberEdit='';

  @override
  void initState() {

    super.initState();
    numberEdit=widget.numberEdit;
  }

  @override
 
  
  Widget build(BuildContext context){
      Size size=MediaQuery.of(context).size;
   
    return Scaffold(
      body:Stack(
        alignment: Alignment.bottomCenter,
        children:<Widget>[
        Container(
          width: double.infinity,
          height: size.height*10/10,
          child:PreferredSize(
        preferredSize: const Size(double.infinity, 230),
        // tang kich thuoc chieu cao của appBar
        // here the desired height
        child: AppBar(
          
          flexibleSpace: Padding(
            padding: const EdgeInsets.all(80),
            //chia khoang cach tat ca deu bang 80
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    SvgPicture.asset('assets/a.svg'),
                    SvgPicture.asset('assets/b.svg'),
                  ],
                ),
                Column(
                  children: const [
                    Text(
                      'Recovered',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                    Text(
                      'Health',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ),
          backgroundColor: Color.fromRGBO(4, 25, 103, 1),
          
        ),
      ),
        ),
        Container(
          width: double.infinity,
          height: size.height *7/10,
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
          color: Colors.white),
          
          child:Wrap(
        alignment: WrapAlignment.spaceBetween,
        children: [
          Column(children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(51, 30, 52, 18),
            ),
            Text(
              'Enter Your Phone Number',
              style: TextStyle(fontSize: 20, color: Color.fromRGBO(4, 25, 103, 1)),
            ),
            Wrap(
              direction: Axis.vertical,
              alignment: WrapAlignment.spaceBetween,
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(51, 10, 52, 16),
            ),
            RichText(
              text: const TextSpan(
                text: 'We will send you the  ',
                style: TextStyle(color: Colors.black, fontSize: 15),
                children: <TextSpan>[
                  TextSpan(
                      text: '6 digit ',
                      style: TextStyle(color: Color.fromRGBO(4, 25, 103, 1), fontSize: 15)),
                  TextSpan(
                    text: 'verification',
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  )
                ],
              ),
            ),
            Wrap(
              direction: Axis.vertical,
              alignment: WrapAlignment.spaceBetween,
            ),
            const Text('code'),
           Padding(padding: EdgeInsets.only(left: 51,right: 51),
           child:Container(
              width: double.infinity,
              height: 60,
              decoration: BoxDecoration(
                color: Colors.white,
                
                borderRadius: BorderRadius.circular(24),
              ),
              child:TextFormField(
              style: TextStyle(color: Color.fromRGBO(4, 25, 103, 1),fontSize: 16,),
              textAlignVertical:TextAlignVertical.bottom,
              keyboardType: TextInputType.numberWithOptions(),
              controller: phoneController,
              maxLength: 12,
              
              onChanged: (value) {
                setState(() {
                  numberEdit=value; 
                });
                
                
              },
              decoration:InputDecoration(
                 
                  
                  prefixIcon:Container(
                     padding: EdgeInsets.symmetric(horizontal: 5,vertical: 2),
                     margin: EdgeInsets.symmetric(horizontal: 8),
                    child:
                    Row(
                     mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.phone),
                        GestureDetector(
                          onTap: () async{
                          final code=  await contryPicker.showPicker(context: context);
                          setState(() {
                            countryCode=code;
                          });
                          },
                          child:Container(
                       padding: EdgeInsets.symmetric(vertical: 5,horizontal: 5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        
                       
                      ),
                      child:Row(children: [
                        Text(countryCode?.dialCode??"(+098)",style: TextStyle(color: Color.fromRGBO(4, 25, 103, 1),fontSize: 16),),
                        Padding(padding: EdgeInsets.symmetric(horizontal: 10,
                        
                        ),
                        child: Icon(Icons.arrow_drop_down,color:Colors.black,),
                        ),
                      ],) 
                      
                     ) ,
                     
                        ),
                     
                     
                    ],)
                    
                  ) ,
                ),
            ), 
            ), 
            ),
            
            
           
          
           
            ButtonTheme(
              minWidth: 300.0,
              height: 40.0,
              child: ElevatedButton(

                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => DetaildScreen(
                        numberEdit:numberEdit,
                        CountryCode:countryCode?.dialCode??"+098",
                      ),
                    ),
                  );
                },
                child: const Text(
                  "NEXT",
                  style: TextStyle(fontSize: 25, color: Colors.white),
                ),
              ),
            ),
          ]),
        ],
      ), 
        ),
        ],
      )

        );
      
    

  }
}

class DetaildScreen extends StatefulWidget {
  @override
String numberEdit;
String CountryCode;
  DetaildScreen({Key? key, required this.numberEdit,
   required this.CountryCode
   })
      : super(key: key);

  _DetailScreen createState() => _DetailScreen();
}

class _DetailScreen extends State<DetaildScreen> with TickerProviderStateMixin {
  
  String numberEdit = '';
  String CountryCode='';

  dynamic counter;
  int _counter1 = 60;
  late AnimationController _controller;
  int levelClock = 60;

  // late Timer _timer;

  // void Start() {
  //   _counter1 = 60;
  //   _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
  //     if (_counter1 > 0) {
  //       setState(() {
  //         _counter1--;
  //       });
  //     } else {
  //       timer.cancel();
  //     }
  //   });
  // }

  @override
  void initState() {
   numberEdit=widget.numberEdit;
   CountryCode=widget.CountryCode;
    super.initState();
    _controller = AnimationController(
        
        vsync: this,
        
        duration: Duration(
            seconds:
                levelClock,) // gameData.levelClock is a user entered number elsewhere in the applciation
        );

    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
     return Scaffold(
      body:Stack(
        alignment: Alignment.bottomCenter,
        children:<Widget>[
        Container(
          width: double.infinity,
          height: size.height*10/10,
          child:PreferredSize(
        preferredSize: const Size(double.infinity, 230),
        // tang kich thuoc chieu cao của appBar
        // here the desired height
        child: AppBar(
          
          flexibleSpace: Padding(
            padding: const EdgeInsets.all(80),
            //chia khoang cach tat ca deu bang 80
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    SvgPicture.asset('assets/a.svg'),
                    SvgPicture.asset('assets/b.svg'),
                  ],
                ),
                Column(
                  children: const [
                    Text(
                      'Recovered',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                    Text(
                      'Health',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ),
          backgroundColor: Color.fromRGBO(4, 25, 103, 1),
          
        ),
      ),
        ),
        Container(
          width: double.infinity,
          height: size.height *7/10,
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
          color: Colors.white),
          
        child: 
        Wrap(
        alignment: WrapAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: [
              Row(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_outlined,
                        color: Color.fromRGBO(4, 25, 103, 1),
                      )),
                  Text(
                    'BACK',
                    style: TextStyle(
                      color: Color.fromRGBO(4, 25, 103, 1),
                    ),
                  )
                ],
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(51, 30, 52, 18),
              ),
              Text(
                'OTP Verification',
                style: TextStyle(fontSize: 20, color: Color.fromRGBO(4, 25, 103, 1)),
              ),
              Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceBetween,
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(51, 10, 52, 16),
              ),
              const Text('A verification codes  has been sent'),
              RichText(
                text: TextSpan(
                  text: 'to ',
                  style: const TextStyle(color: Colors.black, fontSize: 15),
                  children: <TextSpan>[
                    TextSpan(
                      text: (' ($CountryCode)'),
                      style: const TextStyle(color: Color.fromRGBO(4, 25, 103, 1), fontSize: 15),
                    ),
                    TextSpan(
                      text: (' - $numberEdit'),
                      style: const TextStyle(color: Color.fromRGBO(4, 25, 103, 1), fontSize: 15),
                    ),
                  ],
                ),
              ),
              Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceBetween,
              ),
              const Padding(padding: EdgeInsets.all(30)),
              Wrap(
                alignment: WrapAlignment.center,
                children: const <Widget>[
                  MyContainer(),
                  MyContainer(),
                  MyContainer(),
                  MyContainer(),
                  MyContainer(),
                  MyContainer(),
                ],
              ),
              ButtonTheme(
                minWidth: 300.0,
                height: 40.0,
                child: ElevatedButton(

                  onPressed: () {},
                  child: const Text(
                    "VERIFY & CONTINUE",
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.all(20)),
              const Text(
                'Send again OTP',
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
              Countdown(
                
                animation: StepTween(
                  begin: levelClock, // THIS IS A USER ENTERED NUMBER
                  end: 0,
                ).animate(_controller),
              ),
              ButtonTheme(
                minWidth: 300.0,
                height: 40.0,
                child: TextButton(
                  onPressed: () {
                    levelClock = 60;
                    _controller.dispose();
                    _controller = AnimationController(
                        vsync: this,
                        duration: Duration(
                            seconds:
                                levelClock) // gameData.levelClock is a user entered number elsewhere in the applciation
                        );

                    _controller.forward();
                    setState(() {});
                  },
                  child: const Text(
                    "Resent Code",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color.fromRGBO(4, 25, 103, 1),
                        decoration: TextDecoration.underline),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      ),
        ]
      ), 
    );
  }
}

// class ArgumentScreen {
//   String _number1;
//   int _counter;
//   ArgumentScreen(this._number1, this._counter);
// }

class MyContainer extends StatelessWidget {
  const MyContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(7),
      width: 50,
      height: 50,
      color: Colors.white,
      child: const TextField(
        maxLength: 1,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 25, color: Colors.black),
        keyboardType: TextInputType.numberWithOptions(),
        decoration: InputDecoration(hintText: '-'),
      ),
    );
  }
}

class Countdown extends AnimatedWidget {
  Countdown({Key? key, required this.animation})
      : super(key: key, listenable: animation);
  Animation<int> animation;

  @override
  build(BuildContext context) {
    Duration clockTimer = Duration(seconds: animation.value);

    String timerText =
        '${clockTimer.inMinutes.remainder(60).toString()}:${(clockTimer.inSeconds.remainder(60) % 60).toString().padLeft(2, '0')}';

    return Text(
      timerText,
      style: TextStyle(
        fontSize: 40,
        color: Theme.of(context).primaryColor,
      ),
    );
  }
}
